function upload_background() {
    var background_canvas = document.getElementById('background');
    var ctx = background_canvas.getContext('2d');
    var background_image = new Image();
    URL.revokeObjectURL(background_image.src);
    background_image.src = URL.createObjectURL(document.getElementById('create_new').files[0]);
    background_image.onload = function() {
        var hRatio = background_canvas.width / background_image.width;
        var vRatio = background_canvas.height / background_image.height;
        var ratio = Math.min(hRatio, vRatio);
        var W = background_image.width * ratio;
        var H = background_image.height * ratio;

        var left = background_canvas.width / 2 - W / 2;
        var top = background_canvas.height / 2 - H / 2;

        ctx.drawImage(background_image, left, top, background_image.width * ratio, background_image.height * ratio);
    };

    document.getElementById('proxy_create_new').style.display = 'none';
    // document.getElementById('draw_rectangle').style.display = 'block';
    instruction_part2();
};

function instruction_part2() {
    var modal1 = document.getElementById('instruction_part2');
    var span1 = document.getElementById('span1');
    var btndraw_rectangle = document.getElementById('btndraw_rectangle');

    modal1.style.display = "block";

    span1.onclick = function() {
        draw_rectangle();
        modal1.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal1) {
            draw_rectangle();
            modal1.style.display = "none";
        }
    }
    btndraw_rectangle.onclick = function() {
        draw_rectangle();
        modal1.style.display = "none";
    }
};

width = 0;
height = 0;

function draw_rectangle() {
    var rectangle = document.getElementById('wall_measure');
    var ctxrec = rectangle.getContext('2d');
    var rectanglex = document.getElementById('wall_measure').offsetLeft;
    var rectangley = document.getElementById('wall_measure').offsetTop;
    var last_mousex = last_mousey = 0;
    var mousex = mousey = 0;
    var mousedown_flag = false;

    document.getElementById('wall_measure').addEventListener('mousedown', function(e) {
        last_mousex = parseInt(e.clientX - rectanglex);
        last_mousey = parseInt(e.clientY - rectangley);
        mousedown_flag = true;
    });

    document.getElementById('wall_measure').addEventListener('mouseup', function(e) {
        mousedown_flag = false;
    });

    document.getElementById('wall_measure').addEventListener('mousemove', function(e) {
        mousex = parseInt(e.clientX - rectanglex);
        mousey = parseInt(e.clientY - rectangley);
        if (mousedown_flag) {
            ctxrec.clearRect(0, 0, 700, 400); //clear canvas
            ctxrec.globalAlpha = 0.2;
            ctxrec.beginPath();
            width = mousex - last_mousex;
            height = mousey - last_mousey;
            ctxrec.fillRect(last_mousex, last_mousey, width, height);
            ctxrec.strokeStyle = 'blue';
            ctxrec.lineWidth = 2;
            ctxrec.stroke();
        }
    });
    document.getElementById('accept_rectangle').style.display = 'block';
}

function modal_window1() {

    var modal = document.getElementById('rectangle_atributes');
    var accept_measures = document.getElementById('accept_measures');
    var span2 = document.getElementById('span2');

    modal.style.display = "block";

    span2.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    hrectangle = 0;
    wrectangle = 0;
    accept_measures.onclick = function() {
        hrectangle = document.getElementById('height_rectangle').value;
        wrectangle = document.getElementById('width_rectangle').value;

        modal.style.display = "none";

        document.getElementById('wall_measure').style.display = 'none';
        document.getElementById('painting').style.display = 'block';
        document.getElementById('accept_rectangle').style.display = 'none';
        // document.getElementById('proxy_paste_painting').style.display = 'block';
        instruction_part3()
    }
};

function instruction_part3() {

    var modal2 = document.getElementById('instruction_part3');
    // var btnpaste_painting = document.getElementById('paste_painting');
    var btnpaste_painting = document.getElementById('proxy_paste_painting');
    var span3 = document.getElementById('span3');

    modal2.style.display = "block";

    span3.onclick = function() {
        modal2.style.display = "none";
    }
    window.onclick = function(event) {
            if (event.target == modal2) {
                modal2.style.display = "none";
            }
        }
        // btnpaste_painting.onclick = function() {
        //     if (document.getElementById('paste_painting').click()) {
        //         // onclick="document.getElementById('paste_painting').click(); instruction_part4()
        //         modal2.style.display = "none";
        //         instruction_part4();
        //     };
        // }
        //do usuniecia
    btnpaste_painting.onclick = function() {
        document.getElementById('paste_painting').click();
        modal2.style.display = "none";


        //koniec do usuniecia
    };
    document.getElementById('paste_painting').addEventListener('change', instruction_part4, true);
};

function instruction_part4() {

    var modal3 = document.getElementById('instruction_part4');
    var yesedit = document.getElementById('yes');
    var noedit = document.getElementById('no');
    var span4 = document.getElementById('span4');

    modal3.style.display = "block";

    span4.onclick = function() {
        modal3.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal3) {
            modal3.style.display = "none";
        }
    }
    yesedit.onclick = function() {
        modal3.style.display = "none";
        crop_editor(); //change to edit function when it will be written
    }
    noedit.onclick = function() {
        modal3.style.display = "none";
        upload_painting();
    }
};

var painting_image; // = document.querySelector('#paste_painting');
var imgSrc;

function crop_editor() {
    document.getElementById('paste_painting').removeEventListener('change', instruction_part4, true); //new
    painting_image = document.querySelector('#paste_painting'); //new
    var modal_crop_editor = document.getElementById('crop_editor');
    let result = document.querySelector('.result'),
        img_result = document.querySelector('.img-result'),
        img_w = document.querySelector('.img-w'),
        img_h = document.querySelector('.img-h'),
        options = document.querySelector('.options'),
        save = document.querySelector('.save'),
        crop = document.querySelector('.crop'),
        cropped = document.querySelector('.cropped'),
        // dwn = document.querySelector('.download'),
        // upload = document.querySelector('#paste_painting')
        cropper = '';
    // console.log(upload);

    modal_crop_editor.style.display = "block";


    // on change show image with crop options
    painting_image.addEventListener('change', (e) => {
        if (e.target.files.length) {
            // start file reader
            const reader = new FileReader();
            reader.onload = (e) => {
                if (e.target.result) {
                    // create new image
                    let img = document.createElement('img');
                    img.id = 'image';
                    img.src = e.target.result
                        // clean result before
                    result.innerHTML = '';
                    // append new image
                    result.appendChild(img);
                    // show save btn and options
                    save.classList.remove('hide');
                    crop.classList.remove('hide');
                    options.classList.remove('hide');
                    // init cropper
                    cropper = new Cropper(img);
                }
            };
            reader.readAsDataURL(e.target.files[0]);
        }
    });

    painting_image.dispatchEvent(new Event("change"));
    // save on click
    crop.addEventListener('click', (e) => {
        e.preventDefault();
        // get result to data uri
        imgSrc = cropper.getCroppedCanvas({
            width: img_w.value // input value
        }).toDataURL();
        // remove hide class of img
        cropped.classList.remove('hide');
        img_result.classList.remove('hide');
        // show image cropped
        cropped.src = imgSrc;
        // dwn.classList.remove('hide');
        // dwn.download = 'imagename.png';
        // dwn.setAttribute('href', imgSrc);
        painting_image.src = imgSrc; //new update .src

    });
    save.onclick = function() {
        // painting_image = imgSrc;
        modal_crop_editor.style.display = "none";
        upload_painting(imgSrc);
    }
};


var idInterval;
var ctxpt;

function upload_painting(croppedImageSrc = null) {
    document.getElementById('additionals').style.display = 'block';
    var checkframe = document.getElementById("frame");
    var checkshadow = document.getElementById("shadow");
    var painting = document.getElementById('painting');
    ctxpt = painting.getContext('2d');
    // if (typeof painting_image === 'undefined') { //new
    console.log(typeof(painting_image));
    painting_image = new Image();

    var isDraggable = false;
    var hpainting = 0;
    var wpainting = 0;
    var painting_width = 0;
    var painting_height = 0;
    var currentX = painting.width / 2;
    var currentY = painting.height / 2;
    URL.revokeObjectURL(painting_image.src);
    // painting_image.src = URL.createObjectURL(painting_image.files[0]);
    // }
    if (!croppedImageSrc) { //new
        painting_image.src = URL.createObjectURL(document.getElementById('paste_painting').files[0]);
    } else {
        painting_image.src = croppedImageSrc;
    }

    painting_image.onload = function() {
        set_measures();
        drag_and_move();
        idInterval = setInterval(function() {
            reset_and_draw_painting()
        }, 1000 / 30)
    };

    function set_measures() {
        var modal2 = document.getElementById('painting_atributes');
        modal2.style.display = "block";
        var accept_painting_measures = document.getElementById('accept_painting_measures');
        var span5 = document.getElementById('span5');
        span5.onclick = function() {
            modal2.style.display = "none";
        };

        accept_painting_measures.onclick = function() {
            hpainting = document.getElementById('height_painting').value;
            wpainting = document.getElementById('width_painting').value;
            modal2.style.display = "none";
            painting_width = (wpainting * width) / wrectangle;
            painting_height = (hpainting * height) / hrectangle;
        };
    };

    function drag_and_move() {
        painting.onmousedown = function(e) {
            var mouseX = e.pageX - this.offsetLeft;
            var mouseY = e.pageY - this.offsetTop;
            if (mouseX >= (currentX - painting_width / 2) &&
                mouseX <= (currentX + painting_width / 2) &&
                mouseY >= (currentY - painting_height / 2) &&
                mouseY <= (currentY + painting_height / 2)) {
                isDraggable = true;
            }
        };
        painting.onmousemove = function(e) {
            if (isDraggable) {
                currentX = e.pageX - this.offsetLeft;
                currentY = e.pageY - this.offsetTop;
            }
        };
        painting.onmouseup = function(e) {
            isDraggable = false;
        };
        painting.onmouseout = function(e) {
            isDraggable = false;
        };
    }

    // function reset_and_draw_painting() {
    //     ctxpt.clearRect(0, 0, 700, 400);
    //     ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);
    //     checkframe.onclick = function(e) {
    //         if (checkframe.checked == true) {
    //             ctxpt.beginPath();
    //             ctxpt.lineWidth = "6";
    //             ctxpt.strokeStyle = "red";
    //             ctxpt.rect((currentX - (painting_width / 2)) + 2, (currentY - (painting_height / 2)) + 2, painting_width + 2, painting_height + 2);
    //             ctxpt.stroke()
    //         };
    //     };
    // };

    function reset_and_draw_painting() {
        if ((checkframe.checked) && (checkshadow.checked)) {
            ctxpt.clearRect(0, 0, 700, 400);

            ctxpt.beginPath();
            ctxpt.lineWidth = "5";
            ctxpt.strokeStyle = "black";
            ctxpt.rect((currentX - (painting_width / 2)) - 3, (currentY - (painting_height / 2)) - 3, painting_width + 6, painting_height + 6);
            ctxpt.stroke();

            ctxpt.shadowColor = 'black';
            ctxpt.shadowBlur = 10;
            ctxpt.shadowOffsetX = 3;
            ctxpt.shadowOffsetY = 3;

            ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);
        } else {
            if ((checkframe.checked) && (!checkshadow.checked)) {
                ctxpt.clearRect(0, 0, 700, 400);

                ctxpt.beginPath();
                ctxpt.lineWidth = "5";
                ctxpt.strokeStyle = "black";
                ctxpt.rect((currentX - (painting_width / 2)) - 3, (currentY - (painting_height / 2)) - 3, painting_width + 6, painting_height + 6);
                ctxpt.stroke();
                ctxpt.shadowColor = 'transparent';

                ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);
            } else {
                if ((!checkframe.checked) && (checkshadow.checked)) {
                    ctxpt.clearRect(0, 0, 700, 400);

                    ctxpt.fillRect((currentX - (painting_width / 2)), (currentY - (painting_height / 2)), painting_width, painting_height);


                    ctxpt.shadowColor = 'black';
                    ctxpt.shadowBlur = 10;
                    ctxpt.shadowOffsetX = 3;
                    ctxpt.shadowOffsetY = 3;

                    ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);

                    //old monikas
                    // ctxpt.clearRect(0, 0, 700, 400);
                    // console.log("just shadow");


                    // ctxpt.shadowColor = 'black';
                    // ctxpt.shadowBlur = 10;
                    // ctxpt.shadowOffsetX = 3;
                    // ctxpt.shadowOffsetY = 3;


                    // 
                    // ctxpt.lineWidth = "5";
                    // ctxpt.strokeStyle = "transparent";
                    // ctxpt.rect((currentX - (painting_width / 2)) - 3, (currentY - (painting_height / 2)) - 3, painting_width + 6, painting_height + 6);

                    // ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);
                } else {
                    ctxpt.clearRect(0, 0, 700, 400);
                    console.log("nth");
                    ctxpt.drawImage(painting_image, currentX - (painting_width / 2), currentY - (painting_height / 2), painting_width, painting_height);

                    // ctxpt.fill();
                };
            };
        };
    };
    clearInterval(idInterval);
};
// function reset_canvas() {
//     var background_canvas = document.getElementById('background');
//     var ctx = background_canvas.getContext('2d');
//     var rectangle = document.getElementById('wall_measure');
//     var ctxrec = rectangle.getContext('2d');
//     var painting = document.getElementById('painting');
//     var ctxpt = painting.getContext('2d');
//     clearInterval(idInterval);

//     ctx.clearRect(0, 0, background_canvas.width, background_canvas.height);
//     ctxrec.clearRect(0, 0, rectangle.width, rectangle.height);
//     ctxpt.clearRect(0, 0, painting.width, painting.height);

//     document.getElementById('proxy_create_new').style.display = 'block';
//     document.getElementById('accept_rectangle').style.display = 'none';
//     document.getElementById('background').style.display = 'block';
//     document.getElementById('wall_measure').style.display = 'block';
//     document.getElementById('painting').style.display = 'none';
//     document.getElementById('paste_painting').style.display = 'none';
//     document.getElementById('proxy_paste_painting').style.display = 'none';
// }

function download() {
    var background_canvas = document.getElementById('background');
    var painting = document.getElementById('painting');

    var ctx = background_canvas.getContext('2d');
    ctx.drawImage(painting, 0, 0)

    var dataURL = background_canvas.toDataURL("image/png");
    var link = document.createElement('a');
    link.download = "background-painting.png";
    link.href = background_canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    link.click();

};